#!/usr/bin/env python3

import random
import re

mas = [['#' for i in range(8)] for i in range(8)]
mas[3][4] = 'k'
k = [ 3, 4 ]
step = [ [2,1], [2,-1], [-2,1], [-2,-1], [1,-2], [1,2], [-1,2], [-1,-2]]
player = int(input("каким вы желаете ходить? [ 1 | 2 ]"))

while True:
    print(' hgfedcba ')
    i = 1
    for part in mas:
        print(i, end='')
        i = i + 1
        for el in part:
            print(el, end='')
        print()
    
    possible = []
    for s in step:
        mas_borders1 = k[0]+s[0] > -1 and k[0]+s[0] < 8
        mas_borders2 = k[1]+s[1] > -1 and k[1]+s[1] < 8
        if mas_borders1 and mas_borders2 and mas[k[0]+s[0]][k[1]+s[1]] == '#':
            possible.append(s)
    if not possible:
        text = "игрока"
        if player == 2:
            text = "бота"
        print('не осталось ходов для ', text)
        break
    else:
        if player == 1:
            input_data = input()
            if input_data == 'exit':
                break
            if re.match(r'[a-h]{1}[1-8]{1}', input_data) and len(input_data) == 2:
                in1 = input_data[0]
                in2 = int(input_data[1]) - 1
                if in1 == 'h': 
                    in1 = 0
                elif in1 == 'g':
                    in1 = 1
                elif in1 == 'f':
                    in1 = 2
                elif in1 == 'e':
                    in1 = 3
                elif in1 == 'd':
                    in1 = 4
                elif in1 == 'c':
                    in1 = 5
                elif in1 == 'b':
                    in1 = 6
                elif in1 == 'a':
                    in1 = 7
            else:
                print('неверный ввод')
                continue
            player = 2
        else:
            rand_choice = random.choice(possible)
            in1 = k[1] + rand_choice[1]
            in2 = k[0] + rand_choice[0]
            player = 1
        flag = True
        for s in possible:
            if (k[0]+s[0] == in2) and (k[1]+s[1] == in1):
                mas[k[0]][k[1]] = 'O'
                mas[in2][in1] = 'k'
                k = [ in2, in1]
                flag = False
                break
        if flag:
            print('ты не можешь так ходить')
            player = 1
